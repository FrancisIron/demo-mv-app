package Funcionalidad1;

import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Stack;


public class Main {

    public static String csrf(){
        JSONObject csrf = Unirest.get("https://www.bolsadesantiago.com/api/Securities/csrfToken")
                .asJson()
                .getBody()
                .getObject();
        return csrf.get("csrf").toString();
    }

    public static void uf_recover(String start, String end){
        JSONObject uf_value_response = Unirest.post("https://www.bolsadesantiago.com/api/Comunes/getValorUF")
                .header("X-CSRF-Token",csrf())
                .header("Content-Type", "application/json")
                .body("{\"fechafin\":\""+end+"\",\"fechaini\":\""+start+"\"}")
                .asJson()
                .getBody()
                .getObject();
        JSONArray a = (JSONArray) uf_value_response.get("listaResult");
        for (Object as: a) {
            JSONObject inside_json = (JSONObject) as;
            System.out.println("Fecha: "+inside_json.get("FECHA").toString()+" Valor: "+inside_json.get("VALOR").toString());
        }
    }


    public static void uf_web_scrap() throws IOException {
        Document doc = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
        Element tbody = doc.select("tbody").first();
        int count_month = 0;
        Stack day = new Stack();
        for(Element row : tbody.select("tr") ){
            for(Element tds : row.select("td")) {
                Elements tmp_data = tds.getElementsByTag("span");
                if (count_month == 10){
                    day.push(tmp_data.text());
                    //System.out.println(tmp_data);
                    //System.out.println("---------------------------------");
                }
                count_month++;
            }
            count_month =0;
        }
        System.out.println(day.elementAt(12));
        System.out.println(day);
    }

    public static void main(String[] args) throws IOException {

        uf_web_scrap();
        //uf_recover("2020-03-15", "2020-03-15");
    }


}
